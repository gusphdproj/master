# DeepARG 

This is the main repository of the deepARG project. Follow the instructions in this file to install/deploy the web service to a server.

## Deploy webserver
start by clonning the master repo:

    git clone https://bitbucket.org/gusphdproj/master

### microservices

    git clone https://bitbucket.org/gusphdproj/upload_file
    git clone https://bitbucket.org/gusphdproj/cluster_computing

### deepARG stuff

    git clone https://bitbucket.org/gusphdproj/fontend-analyze
    git clone https://bitbucket.org/gusphdproj/web_deepargs

### Docker-compose 

    docker-compose build
    docker-compose -p deeparg up -d

## Create the deeparg front end (analyze)

    ng build --base-href /deeparg_analyze/